/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include "../src/tinytag.h"

using namespace std;
using namespace cv;

int main(int   argc,
         char *argv[])
{
    TinyTagDetector detector;

    string inPath, outPath;
    bool   inverse = false;

    if (argc < 2)
    {
        cerr << "USAGE:\n\tprint_tags -i img [-x]\n" << endl;

        return -1;
    }

    for (int i = 1; i < argc; ++i)
    {
        if (string(argv[i]) == "-x") { inverse = true; }
        else if (string(argv[i]) == "-i")
        {
            inPath = argv[i + 1];
            ++i;
        }
        else if (string(argv[i]) == "-o")
        {
            outPath = argv[i + 1];
            ++i;
        }
    }

    Mat image = imread(inPath);
    if (image.empty())
    {
        cerr << "could not load " << inPath << endl;
    }

    if (inverse)
    {
        image ^= Scalar::all(255);
    }

    int64 t;
    t = getTickCount();
    // vector of TinyTag::Tag that will be resized by the detectTags function
    vector<TinyTagInfo> tags;
    detector.detectTags(tags, image);
    t = getTickCount() - t;
    fprintf(stderr, "Time elapsed: %fms Found %d tags\n",
            t * 1000 / getTickFrequency(), static_cast<int>(tags.size()));

    ostream *fs;
    if (outPath.empty()) { fs = &cout; }
    else { fs = new ofstream(outPath.c_str()); }

    int i = 0;

    vector<TinyTagInfo>::const_iterator itE = tags.end();
    for (vector<TinyTagInfo>::const_iterator it = tags.begin(); it != itE;
         ++it)
    {
        // cout << "Tag " << i << " center pos : [" << it->center.x << "," <<
        // it->center.y << "] id : " << it->id << endl;
        (*fs) << it->id() << " " << it->center().x << " " << it->center().y << endl;
        ++i;
    }

    return 0;
}
