/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <cstdio>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include <mvg/logger.h>
#include "../src/tinytag.h"

using namespace std;
using namespace cv;

int main(int   argc,
         char *argv[])
{

    string path;
    bool   inverse = false;
    size_t n       = 5;
    size_t k       = 9;

    if (argc < 2)
    {
        cerr << "USAGE:\n\tdetect_tags -i img [-x -n n -k k]\n" << endl;

        return -1;
    }

    for (int i = 1; i < argc; ++i)
    {
        if (string(argv[i]) == "-x") { inverse = true; }
        else if (string(argv[i]) == "-i")
        {
            if (i + 1 < argc)
            {
                path = argv[i + 1];
                ++i;
            }
            else
            {
                logError() << "Missing filename";
            }
        }
        else if (string(argv[i]) == "-n")
        {
            if (i + 1 < argc)
            {
                if (!(istringstream(argv[i + 1]) >> n))
                {
                    logError() << "-n expects an integer";
                }
                ++i;
            }
            else
            {
                logError() << "Missing number of bits";
            }
        }
        else if (string(argv[i]) == "-k")
        {
            if (i + 1 < argc)
            {
                if (!(istringstream(argv[i + 1]) >> k))
                {
                    logError() << "-k expects an integer";
                }
                ++i;
            }
            else
            {
                logError() << "Missing number of bits";
            }
        }
    }

    Mat image = imread(path, 0);
    if (image.empty())
    {
        cerr << "could not load " << path << endl;
    }

    if (inverse)
    {
        image ^= Scalar::all(255);
    }

    TinyTagDetector     detector(n, k);
    vector<TinyTagInfo> tags;
    int64               t = getTickCount();
    detector.detectTags(tags, image);
    t = getTickCount() - t;
    fprintf(stderr, "Time elapsed: %fms Found %d tags\n",
            t * 1000 / getTickFrequency(), static_cast<int>(tags.size()));

    Mat dImage = drawTags(image, tags);

    /*
     *   //only for pimage.png ...
     *   map<int, vector<Point2f> > proj_tags;
     *   vector<TinyTag::Tag>::const_iterator it, ite;
     *   for (it = tags.begin(), ite = tags.end(); it != ite; ++it) {
     *    int id = it->id;
     *    int y = 1+it->id/15; y *= (28+21);
     *    int x = 1+it->id%15; x *= (28+21);
     *    cout << "found : ";
     *    for (int i=0; i<4; ++i) cout << "[" << it->corners[i].x << "," << it->corners[i].y << "]
     * ";
     *    cout << "center : (" << it->center.x << "," << it->center.y << ")";
     *    cout << endl;
     *    cout << "got : ";
     *    cout << "[" << x-0.5 << "," << y-0.5 << "] ";
     *    cout << "[" << x-0.5+21 << "," << y-0.5 << "] ";
     *    cout << "[" << x-0.5+21 << "," << y-0.5+21 << "] ";
     *    cout << "[" << x-0.5 << "," << y-0.5+21 << "] ";
     *    cout << "center : (" << x-0.5+21/2. << "," << y-0.5+21/2. << ")";
     *    cout << endl;
     *    getchar();
     *   }
     */

    namedWindow("tags", WINDOW_NORMAL);
    imshow("tags", dImage);
    waitKey(0);

    return 0;
}
