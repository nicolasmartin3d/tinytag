#
# TinyTag - A library to detect and print simple fiducial markers.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of TinyTag.
#
# TinyTag is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# TinyTag is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
#

REMOVE_DEFINITIONS(-DTINYTAG_BUILDING)

ADD_EXECUTABLE( detect_tags detect_tags.cpp)
TARGET_LINK_LIBRARIES( detect_tags tinytag)

ADD_EXECUTABLE( print_tags print_tags.cpp)
TARGET_LINK_LIBRARIES( print_tags tinytag)

ADD_EXECUTABLE( print_projector_tags print_projector_tags.cpp)
TARGET_LINK_LIBRARIES( print_projector_tags tinytag)

ADD_EXECUTABLE( make_tags_image make_tags_image.cpp)
TARGET_LINK_LIBRARIES( make_tags_image tinytag)

find_package(OpenCV COMPONENTS highgui videoio)
if (OPENCV_VIDEOIO_FOUND)
	ADD_EXECUTABLE( capture_tags capture_tags.cpp)
	INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIRS})
	TARGET_LINK_LIBRARIES(capture_tags tinytag ${OpenCV_LIBS})
endif (OPENCV_VIDEOIO_FOUND)

INSTALL(TARGETS detect_tags RUNTIME DESTINATION bin)
INSTALL(TARGETS print_tags RUNTIME DESTINATION bin)
INSTALL(TARGETS print_projector_tags RUNTIME DESTINATION bin)
INSTALL(TARGETS make_tags_image RUNTIME DESTINATION bin)

# those need calib3d module for projectPoints .. (to change soon)
find_package(OpenCV COMPONENTS calib3d)
if (OPENCV_CALIB3D_FOUND)
	ADD_EXECUTABLE( calibrate_with_tags calibrate_with_tags.cpp)
	TARGET_LINK_LIBRARIES( calibrate_with_tags tinytag ${OpenCV_LIBS})
	
	INSTALL(TARGETS calibrate_with_tags RUNTIME DESTINATION bin)
endif (OPENCV_CALIB3D_FOUND)

