/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <map>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../src/tinytag.h"

using namespace std;
using namespace cv;

inline ushort posToShort(double pos,
                         int    size)
{
    pos = (pos < 0 ? 0 : (pos > size - 1 ? size - 1 : pos));

    return static_cast<ushort>((pos / (size - 1)) * 65535. + 0.5);
}

inline double shortToPos(ushort val,
                         int    size)
{
    return (val / 65535.) * (size - 1);
}

int main(int   argc,
         char *argv[])
{

    vector<Mat>     images;
    vector<Mat>     cmaps;
    Size            projSize;
    TinyTagDetector detector;

    for (int i = 1; i < argc; i++)
    {
        const char *s = argv[i];
        if (strcmp(s, "-map") == 0)
        {
            Mat tmp = imread(argv[i + 1], -1);
            if (tmp.empty())
            {
                cerr << "could not load " << argv[i + 1] << endl;
            }
            cmaps.push_back(tmp);
            ++i;
        }
        else if (strcmp(s, "-w") == 0)
        {
            projSize.width = atoi(argv[++i]);
        }
        else if (strcmp(s, "-h") == 0)
        {
            projSize.height = atoi(argv[++i]);
        }
        else if (strcmp(s,  "-im") == 0)
        {
            Mat tmp = imread(argv[i + 1], -1);
            if (tmp.empty())
            {
                cerr << "could not load " << argv[i + 1] << endl;
            }
            images.push_back(tmp);
            ++i;
        }
    }

    const size_t                nbCams = images.size();
    vector< map<int, Point2f> > pts2d(nbCams);
    vector<TinyTagInfo>         tags;
    for (size_t i = 0; i < nbCams; ++i)
    {
        detector.detectTags(tags, images[i]);
        size_t tsize = tags.size();
        for (size_t j = 0; j < tsize; ++j)
        {
            pts2d[i][tags[j].id()] = tags[j].center();
        }
    }

    map<int, int> ids;
    for (size_t i = 0; i < nbCams; ++i)
    {
        for (map<int, Point2f>::const_iterator it = pts2d[i].begin(),
             itE = pts2d[i].end();
             it != itE; ++it)
        {
            ids[it->first]++;
        }
    }

    // points dans le proj
    for (map<int, int>::const_iterator it = ids.begin(), itE = ids.end();
         it != itE; ++it)
    {
        if (it->second != static_cast<int>(nbCams)) { continue; }

        const int       id = it->first;
        vector<Point2f> pts(nbCams);
        for (size_t i = 0; i < nbCams; ++i)
        {
            Point2f p = pts2d[i][id];
            Mat     patch;
            Vec3w   val;

            remap(cmaps[i], patch, Mat(1, 1, CV_32FC2, &p),
                  noArray(), INTER_LINEAR, BORDER_REFLECT_101);
            val      = patch.at<Vec3w>(0, 0);
            pts[i].x = static_cast<float>(shortToPos(val[2], projSize.width));
            pts[i].y = static_cast<float>(shortToPos(val[1], projSize.height));
        }
        Scalar s = mean(Mat(pts));
        cout << id << " " << s[0] << " " << s[1] << endl;
    }

    return 0;
}
