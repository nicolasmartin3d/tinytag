/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <cstdio>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#include <opencv2/videoio/videoio.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include "../src/tinytag.h"

#define TEMP_FIX
#if !defined _MSC_VER && !defined TEMP_FIX
#error Fix this program and replace getopt by TCLAP
#else
extern char *optarg;
char getopt(int,
            char      **,
            const char *) { return 0; }
#endif

using namespace std;
using namespace cv;

int main(int   argc,
         char *argv[])
{
    TinyTagDetector     detector;
    vector<TinyTagInfo> tags;
    Mat                 dImage;

    int    index     = 0;
    bool   inverse   = false;
    double threshold = 0;
    int    countTags = 15;
    int    c;
    while ((c = getopt(argc, argv, "in:t:c:")) != -1)
    {
        switch (c)
        {
            case 'i': {
                inverse = true;
                break;
            }
            case 'c': {
                if (!(istringstream(optarg) >> countTags))
                {
                    cerr << "Option -c : integer argument expected" << endl;

                    return -1;
                }
                if (countTags < 0)
                {
                    cout << "Option -c : count can't be negative" << endl;

                    return -1;
                }
                break;
            }
            case 'n': {
                if (!(istringstream(optarg) >> index))
                {
                    cerr << "Option -n : integer argument expected" << endl;

                    return -1;
                }
                if (index < 0)
                {
                    cout << "Option -n : index can't be negative" << endl;

                    return -1;
                }
                break;
            }
            case 't': {
                if (!(istringstream(optarg) >> threshold))
                {
                    cerr << "Option -t : real argument expected" << endl;

                    return -1;
                }
                if (index < 0)
                {
                    cout << "Option -t : threshold can't be negative" << endl;

                    return -1;
                }
                break;
            }
            case 'h': {
                // showHelpAndDie();
                break;
            }
        }
    }

    VideoCapture cap(index);
    if (!cap.isOpened())
    {
        cerr << "Could not open Prosillica" << endl;
        exit(-1);
    }
#if CV_MAJOR_VERSION >= 3
    cap.set(CAP_PROP_MONOCHROME, 1);
#else
    cap.set(CV_CAP_PROP_MONOCROME, 1);
#endif

    vector<Mat> images;
    Mat         frame, lframe;
    for (;;)
    {
        cap >> frame;

        if (frame.empty())
        {
            break;
        }
        if (lframe.empty()) { lframe = frame.clone(); }

        double n = norm(frame - lframe, NORM_L1) / (frame.rows * frame.cols);

        frame.copyTo(lframe);

        if (n > threshold)
        {
            continue;
        }

        if (inverse)
        {
            frame = Mat(frame.size(), CV_8U, Scalar::all(255)) - frame;
        }

        detector.detectTags(tags, frame);
        dImage = drawTags(frame, tags);

        imshow("tags", dImage);
        cout << "found " << tags.size() << " tags" << endl;

        int tsize = static_cast<int>(tags.size());
        if (tsize >= countTags)
        {
            images.push_back(frame);
        }

        c = static_cast<int>(waitKey(tags.size() != 0 ? 500 : 5));
        if (c == 27) { break; }
    }

    size_t count = images.size();
    for (size_t i = 0; i < count; ++i)
    {
        string path = format("img_%04d.png", i);
        imwrite(path, images[i]);
    }

}
