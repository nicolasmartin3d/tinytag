/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>

#include <mvg/mvg.h>
#include <mvg/resect.h>
#include "tinytag.h"

using namespace cv;
using namespace std;

template <size_t n>
void readPointFile(const string &path,
                   vector< Vec<double, n> > &pts,
                   vector<int> &ids)
{
    ifstream ifs(path.c_str());
    string   str;
    while (getline(ifs, str))
    {
        int            id;
        Vec<double, n> vec;
        istringstream  iss(str);
        iss >> id;
        for (int i = 0; i < n; ++i)
        {
            iss >> vec[i];
        }
        pts.push_back(vec);
        ids.push_back(id);
    }
}

const char *usage =
    " \nexample command line for calibration from a live feed.\n"
    "   calibration  -w 4 -h 5 -s 0.025 -o camera.yml -op -oe\n"
    " \n"
    " example command line for calibration from a list of stored images:\n"
    "   imagelist_creator image_list.xml *.png\n"
    "   calibration -w 4 -h 5 -s 0.025 -o camera.yml -op -oe image_list.xml\n"
    " where image_list.xml is the standard OpenCV XML/YAML\n"
    " use imagelist_creator to create the xml or yaml list\n"
    " file consisting of the list of strings, e.g.:\n"
    " \n"
    "<?xml version=\"1.0\"?>\n"
    "<opencv_storage>\n"
    "<images>\n"
    "view000.png\n"
    "view001.png\n"
    "<!-- view002.png -->\n"
    "view003.png\n"
    "view010.png\n"
    "one_extra_view.jpg\n"
    "</images>\n"
    "</opencv_storage>\n";

static void help()
{
    printf("This is a camera calibration sample.\n"
           "Usage: calibration\n"
           "     -w <board_width>         # the number of inner corners per one of board dimension\n"
           "     -h <board_height>        # the number of inner corners per another board dimension\n"
           "     [-s <squareSize>]       # square size in some user-defined units (1 by default)\n"
           "     [-o <out_camera_params>] # the output filename for intrinsic [and extrinsic] parameters\n"
           "     [-op]                    # write detected feature points\n"
           "     [-oe]                    # write extrinsic parameters\n"
           "     [-zt]                    # assume zero tangential distortion\n"
           "     [-a <aspectRatio>]      # fix aspect ratio (fx/fy)\n"
           "     [-p]                     # fix the principal point at the center\n"
           "     [input_data]             # input data, one of the following:\n"
           "                              #  - text file with a list of the images of the board\n"
           "                              #    the text file can be generated with imagelist_creator\n"
           "                              #  - name of video file with a video of the board\n"
           "                              # if input_data not specified, a live view from the camera is used\n"
           "\n");
    printf("\n%s", usage);
}

static double computeReprojectionErrors(const vector<vector<Point3f> > &objectPoints,
                                        const vector<vector<Point2f> > &imagePoints,
                                        const vector<Mat>              &rvecs,
                                        const vector<Mat>              &tvecs,
                                        const Mat                      &cameraMatrix,
                                        const Mat                      &distCoeffs,
                                        vector<float>                  &perViewErrors)
{
    vector<Point2f> imagePoints2;
    int             i, totalPoints = 0;
    double          totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for (i = 0; i < (int)objectPoints.size(); i++)
    {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        cout << Mat(imagePoints[i]) << endl;
        cout << imagePoints2 << endl;
        cout << "****************************************" << endl;
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float)std::sqrt(err * err / n);
        totalErr        += err * err;
        totalPoints     += n;
    }

    return std::sqrt(totalErr / totalPoints);
}

static void calcChessboardCorners(Size               boardSize,
                                  float              stepSize,
                                  float              squareSize,
                                  vector<Point3f>   &corners,
                                  const vector<int> &ids)
{
    vector<Point3f> planeA(boardSize.width * boardSize.height);
    vector<Point3f> planeB(boardSize.width * boardSize.height);

    for (int j = 0, k = 0; j < boardSize.height; j++)
    {
        for (int i = 0; i < boardSize.width; i++, ++k)
        {
            planeB[k] = Point3f(2.5 + j * stepSize + 0.5 * squareSize,
                                i * stepSize + 0.5 * squareSize,
                                0);
        }
    }
    for (int j = 0, k = 0; j < boardSize.height; j++)
    {
        for (int i = boardSize.width - 1; i >= 0; i--, ++k)
        {
            planeA[k] = Point3f(0,
                                i * stepSize + 0.5 * squareSize,
                                2.5 + j * stepSize + 0.5 * squareSize);
        }
    }

    corners.resize(ids.size());
    for (int i = 0; i < ids.size(); ++i)
    {
        if (ids[i] < boardSize.width * boardSize.height)
        {
            corners[i] =
                planeB[ids[i]];
        }
        else
        {
            corners[i] = planeA[ids[i] - boardSize.width * boardSize.height];
        }
    }
}

Mat initializeCamera(const vector<Point2f> &imagePoints,
                     const vector<Point3f> &objectPoints,
                     Size                   boardSize,
                     Size                   imageSize,
                     const vector<int>     &ids)
{
    Mat H1(3, 3, CV_64F), H2(3, 3, CV_64F);
    {
        int count = 0;
        for (int i = 0; i < ids.size(); ++i)
        {
            if (ids[i] < boardSize.width * boardSize.height) { count++; }
        }

        double _A[count * 2], _b[count * 2];
        Mat    A(count, 2, CV_64F, _A), b(count, 2, CV_64F, _b);

        for (int i = 0, j = 0; i < ids.size(); ++i)
        {
            if (ids[i] < boardSize.width * boardSize.height)
            {
                _A[j * 2]     = objectPoints[i].x;
                _A[j * 2 + 1] = objectPoints[i].y;
                _b[j * 2]     = imagePoints[i].x;
                _b[j * 2 + 1] = imagePoints[i].y;
                ++j;
            }
        }
        Mat mask;
        H1 = findHomography(A, b, 0, 3, mask);
        cout << H1 << endl;
    }
    {
        int count = 0;
        for (int i = 0; i < ids.size(); ++i)
        {
            if (ids[i] >= boardSize.width * boardSize.height) { count++; }
        }

        double _A[count * 2], _b[count * 2];
        Mat    A(count, 2, CV_64F, _A), b(count, 2, CV_64F, _b);

        for (int i = 0, j = 0; i < ids.size(); ++i)
        {
            if (ids[i] >= boardSize.width * boardSize.height)
            {
                _A[j * 2]     = objectPoints[i].y;
                _A[j * 2 + 1] = objectPoints[i].z;
                _b[j * 2]     = imagePoints[i].x;
                _b[j * 2 + 1] = imagePoints[i].y;
                ++j;
            }
        }
        Mat mask;
        H2 = findHomography(A, b, 0, 3, mask);
        cout << H2 << endl;
    }

    double  _A[4 * 5];
    Mat     A(4, 5, CV_64F, _A);
    double *h1 = H1.ptr<double>(), *h2 = H2.ptr<double>();
    _A[0 * 5 + 0] = h1[0] * h1[1];
    _A[0 * 5 + 1] = h1[3] * h1[4];
    _A[0 * 5 + 2] = h1[1] * h1[6] + h1[0] * h1[7];
    _A[0 * 5 + 3] = h1[4] * h1[6] + h1[3] * h1[7];
    _A[0 * 5 + 4] = h1[6] * h1[7];
    _A[1 * 5 + 0] = h2[0] * h2[1];
    _A[1 * 5 + 1] = h2[3] * h2[4];
    _A[1 * 5 + 2] = h2[1] * h2[6] + h2[0] * h2[7];
    _A[1 * 5 + 3] = h2[4] * h2[6] + h2[3] * h2[7];
    _A[1 * 5 + 4] = h2[6] * h2[7];
    _A[2 * 5 + 0] = h1[0] * h1[0] - h1[1] * h1[1];
    _A[2 * 5 + 1] = h1[3] * h1[3] - h1[4] * h1[4];
    _A[2 * 5 + 2] = 2 * h1[0] * h1[6] - 2 * h1[1] * h1[7];
    _A[2 * 5 + 3] = 2 * h1[3] * h1[6] - 2 * h1[4] * h1[7];
    _A[2 * 5 + 4] = h1[6] * h1[6] - h1[7] * h1[7];
    _A[3 * 5 + 0] = h2[0] * h2[0] - h2[1] * h2[1];
    _A[3 * 5 + 1] = h2[3] * h2[3] - h2[4] * h2[4];
    _A[3 * 5 + 2] = 2 * h2[0] * h2[6] - 2 * h2[1] * h2[7];
    _A[3 * 5 + 3] = 2 * h2[3] * h2[6] - 2 * h2[4] * h2[7];
    _A[3 * 5 + 4] = h2[6] * h2[6] - h2[7] * h2[7];

    SVD svd(A, SVD::FULL_UV);

    double *_W = svd.vt.row(4).ptr<double>();
    double  s  = _W[1];
    for (int i = 0; i < 5; ++i)
    {
        _W[i] /= s;
    }

    double ar = sqrt(_W[0]);  // ar can't be negative
    double cy = -_W[3];
    double cx = -_W[2] / _W[0];
    double fx = sqrt((-ar * ar * cx * cx - cy * cy + _W[4]) / ar);
    if (fx < 0) { fx = -fx; }

    fx = 2155;
    ar = 1;
    cx = 660;
    cy = 460;

    // OpenCV does not like principal points outside image !
    if ((cx < 0) || (cx > imageSize.width)) { cx = imageSize.width / 2; }
    if ((cy < 0) || (cy > imageSize.height)) { cy = imageSize.height / 2; }

    Mat K(3, 3, CV_64F);
    K = Scalar::all(0);
    double *Kp = K.ptr<double>();
    Kp[0 * 3 + 0] = fx;
    Kp[0 * 3 + 2] = cx;
    Kp[1 * 3 + 1] = ar * fx;
    Kp[1 * 3 + 2] = cy;
    Kp[2 * 3 + 2] = 1;

    return K;
}

static bool runCalibration(const vector< vector<Point3f> > &objectPoints,
                           const vector< vector<Point2f> > &imagePoints,
                           Size                             imageSize,
                           Size                             boardSize,
                           float                            stepSize,
                           float                            squareSize,
                           float                            aspectRatio,
                           int                              flags,
                           Mat                             &cameraMatrix,
                           Mat                             &distCoeffs,
                           vector<Mat>                     &rvecs,
                           vector<Mat>                     &tvecs,
                           vector<float>                   &reprojErrs,
                           double                          &totalAvgErr)
{
    /*
     *   cameraMatrix = Mat::eye(3, 3, CV_64F);
     *   if( flags & CV_CALIB_FIX_ASPECT_RATIO )
     *    cameraMatrix.at<double>(0,0) = aspectRatio;
     */

    // need to initialize the camera when using DLT
    // cameraMatrix = initializeCamera(imagePoints[0], objectPoints[0],
    // boardSize, ids);
    // cameraMatrix = initializeCamera(imagePoints[0], objectPoints[0],
    // boardSize, imageSize, ids);

    cout << Mat(objectPoints[0]) << endl;
    cout << Mat(imagePoints[0]) << endl;

    {
        Mat      pts3d(objectPoints[0].size(), 1, CV_64FC3);
        Mat      pts2d(objectPoints[0].size(), 1, CV_64FC2);
        Point3d *p1 = pts3d.ptr<Point3d>();
        Point2d *p2 = pts2d.ptr<Point2d>();
        for (int i = 0; i < objectPoints[0].size(); ++i)
        {
            p1[i] = objectPoints[0][i];
            p2[i] = imagePoints[0][i];
        }

        vector<bool> mask;
        Mat          M;
        findResectionMatrix(pts3d, pts2d, M, &mask, CV_RANSAC, 1, 0.99);
        cout << M << endl;

        Mat R, t;
        decompositionKRT(M, cameraMatrix, R, t);
    }

    cout << cameraMatrix << endl;
    // no skew !
    cameraMatrix.at<double>(0, 1) = 0;
    distCoeffs                    = Mat::zeros(8, 1, CV_64F);

    double rms = calibrateCamera(objectPoints, imagePoints, imageSize,
                                 cameraMatrix,
                                 distCoeffs, rvecs, tvecs,
                                 flags | CV_CALIB_FIX_K3 | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 |
                                 CV_CALIB_USE_INTRINSIC_GUESS);
    printf("RMS error reported by calibrateCamera: %g\n", rms);

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);
    cout << cameraMatrix << endl;
    cout << distCoeffs << endl;

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                            rvecs, tvecs, cameraMatrix,
                                            distCoeffs, reprojErrs);

    return ok;
}

static void saveCameraParams(const string                   &filename,
                             Size                            imageSize,
                             Size                            boardSize,
                             float                           squareSize,
                             float                           aspectRatio,
                             int                             flags,
                             const Mat                      &cameraMatrix,
                             const Mat                      &distCoeffs,
                             const vector<Mat>              &rvecs,
                             const vector<Mat>              &tvecs,
                             const vector<float>            &reprojErrs,
                             const vector<vector<Point2f> > &imagePoints,
                             double                          totalAvgErr)
{
    FileStorage fs(filename, FileStorage::WRITE);

    time_t tt;
    time(&tt);
    struct tm *t2 = localtime(&tt);
    char       buf[1024];
    strftime(buf, sizeof(buf) - 1, "%c", t2);

    fs << "calibration_time" << buf;

    if (!rvecs.empty() || !reprojErrs.empty())
    {
        fs << "nframes" << (int)std::max(rvecs.size(), reprojErrs.size());
    }
    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    fs << "board_width" << boardSize.width;
    fs << "board_height" << boardSize.height;
    fs << "square_size" << squareSize;

    if (flags & CV_CALIB_FIX_ASPECT_RATIO)
    {
        fs << "aspectRatio" << aspectRatio;
    }

    if (flags != 0)
    {
        sprintf(buf, "flags: %s%s%s%s",
                flags & CV_CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
                flags & CV_CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
                flags & CV_CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
                flags & CV_CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "");
        cvWriteComment(*fs, buf, 0);
    }

    fs << "flags" << flags;

    fs << "camera_matrix" << cameraMatrix;
    fs << "distortion_coefficients" << distCoeffs;

    fs << "avg_reprojection_error" << totalAvgErr;
    if (!reprojErrs.empty())
    {
        fs << "per_view_reprojection_errors" << Mat(reprojErrs);
    }

    if (!rvecs.empty() && !tvecs.empty())
    {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
        for (int i = 0; i < (int)rvecs.size(); i++)
        {
            Mat r = bigmat(Range(i, i + 1), Range(0, 3));
            Mat t = bigmat(Range(i, i + 1), Range(3, 6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            // *.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        cvWriteComment(*fs,
                       "a set of 6-tuples (rotation vector + translation vector) for each view",
                       0);
        fs << "extrinsic_parameters" << bigmat;
    }

    if (!imagePoints.empty())
    {
        Mat imagePtMat((int)imagePoints.size(),
                       (int)imagePoints[0].size(), CV_32FC2);
        for (int i = 0; i < (int)imagePoints.size(); i++)
        {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "image_points" << imagePtMat;
    }
}

static bool readStringList(const string   &filename,
                           vector<string> &l)
{
    l.resize(0);
    FileStorage fs(filename, FileStorage::READ);
    if (!fs.isOpened())
    {
        return false;
    }
    FileNode n = fs.getFirstTopLevelNode();
    if (n.type() != FileNode::SEQ)
    {
        return false;
    }
    FileNodeIterator it = n.begin(), it_end = n.end();
    for (; it != it_end; ++it)
    {
        l.push_back((string) * it);
    }

    return true;
}

static bool runAndSave(const string        &outputFilename,
                       const vector<Vec3d> &pts3d,
                       const vector<int>   &ids3d,
                       const vector<Vec2d> &pts2d,
                       const vector<int>   &ids2d,
                       Size                 imageSize,
                       Size                 boardSize,
                       float                stepSize,
                       float                squareSize,
                       float                aspectRatio,
                       int                  flags,
                       Mat                 &cameraMatrix,
                       Mat                 &distCoeffs,
                       bool                 writeExtrinsics,
                       bool                 writePoints)
{
    vector<Mat>   rvecs, tvecs;
    vector<float> reprojErrs;
    double        totalAvgErr = 0;

    vector< vector<Point3f> > pts3dAll(1, vector<Point3f>(ids2d.size()));
    vector< vector<Point2f> > pts2dAll(1, vector<Point2f>(ids2d.size()));
    for (int i = 0; i < ids2d.size(); ++i)
    {
        // suppose que pts3d est en ordre !
        pts3dAll[0][i] = Point3f(pts3d[ids2d[i]][0],
                                 pts3d[ids2d[i]][1],
                                 pts3d[ids2d[i]][2]);
        pts2dAll[0][i] = Point2f(pts2d[i][0], pts2d[i][1]);
    }

    bool ok = runCalibration(pts3dAll, pts2dAll, imageSize, boardSize, stepSize,
                             squareSize,
                             aspectRatio, flags, cameraMatrix, distCoeffs,
                             rvecs, tvecs, reprojErrs, totalAvgErr);
    printf("%s. avg reprojection error = %.2f\n",
           ok ? "Calibration succeeded" : "Calibration failed",
           totalAvgErr);

#if 0
    if (ok)
    {
        saveCameraParams(outputFilename, imageSize,
                         boardSize, squareSize, aspectRatio,
                         flags, cameraMatrix, distCoeffs,
                         writeExtrinsics ? rvecs : vector<Mat>(),
                         writeExtrinsics ? tvecs : vector<Mat>(),
                         writeExtrinsics ? reprojErrs : vector<float>(),
                         writePoints ? pts2dAll : vector<vector<Point2f> >(),
                         totalAvgErr);
    }
#endif

    Camera cam;
    Mat    R;
    Mat    M(3, 4, CV_64F);
    Mat    sR = M(Range(0, 3), Range(0, 3));
    Mat    st = M(Range(0, 3), Range(3, 4));

    cam.K      = cameraMatrix.clone();
    cam.coeffs = distCoeffs.clone();
    Rodrigues(rvecs[0], R);
    cam.R         = R.clone();
    cam.t         = tvecs[0].clone();
    sR            = cam.K * cam.R;
    st            = cam.K * cam.t;
    cam.M         = M.clone();
    cam.imageSize = imageSize;
    cam.write(outputFilename);

    return ok;
}

int main(int    argc,
         char **argv)
{
    float                    squareSize  = 1.f;
    float                    aspectRatio = 1.f;
    float                    stepSize    = 1.f;
    Mat                      cameraMatrix, distCoeffs;
    const char              *outputFilename = "out_camera_data.yml";
    string                   pts3dPath;
    string                   pts2dPath;
    bool                     writeExtrinsics = false;
    bool                     writePoints     = false;
    int                      flags           = 0;
    vector<vector<Point2f> > imagePoints;
    Size                     imageSize;
    Size                     boardSize;

    if (argc < 2)
    {
        help();

        return 0;
    }

    for (int i = 1; i < argc; i++)
    {
        const char *s = argv[i];
        if (strcmp(s, "-w") == 0)
        {
            if ((sscanf(argv[++i], "%u",
                        &boardSize.width) != 1) || (boardSize.width <= 0))
            {
                return fprintf(stderr, "Invalid board width\n"), -1;
            }
        }
        else if (strcmp(s, "-h") == 0)
        {
            if ((sscanf(argv[++i], "%u",
                        &boardSize.height) != 1) || (boardSize.height <= 0))
            {
                return fprintf(stderr, "Invalid board height\n"), -1;
            }
        }
        else if (strcmp(s, "-W") == 0)
        {
            if ((sscanf(argv[++i], "%u",
                        &imageSize.width) != 1) || (imageSize.width <= 0))
            {
                return fprintf(stderr, "Invalid image width\n"), -1;
            }
        }
        else if (strcmp(s, "-H") == 0)
        {
            if ((sscanf(argv[++i], "%u",
                        &imageSize.height) != 1) || (imageSize.height <= 0))
            {
                return fprintf(stderr, "Invalid image height\n"), -1;
            }
        }
        else if (strcmp(s, "-s") == 0)
        {
            if ((sscanf(argv[++i], "%f",
                        &squareSize) != 1) || (squareSize <= 0))
            {
                return fprintf(stderr, "Invalid board square width\n"), -1;
            }
        }
        else if (strcmp(s, "-t") == 0)
        {
            if ((sscanf(argv[++i], "%f", &stepSize) != 1) || (stepSize <= 0))
            {
                return fprintf(stderr, "Invalid board step width\n"), -1;
            }
        }
        else if (strcmp(s, "-a") == 0)
        {
            if ((sscanf(argv[++i], "%f",
                        &aspectRatio) != 1) || (aspectRatio <= 0))
            {
                return printf("Invalid aspect ratio\n"), -1;
            }
            flags |= CV_CALIB_FIX_ASPECT_RATIO;
        }
        else if (strcmp(s, "-op") == 0)
        {
            writePoints = true;
        }
        else if (strcmp(s, "-oe") == 0)
        {
            writeExtrinsics = true;
        }
        else if (strcmp(s, "-zd") == 0)
        {
            flags |= CV_CALIB_FIX_K1 | CV_CALIB_FIX_K2 |
                     CV_CALIB_ZERO_TANGENT_DIST;
        }
        else if (strcmp(s, "-zt") == 0)
        {
            flags |= CV_CALIB_ZERO_TANGENT_DIST;
        }
        else if (strcmp(s, "-p") == 0)
        {
            flags |= CV_CALIB_FIX_PRINCIPAL_POINT;
        }
        else if (strcmp(s, "-o") == 0)
        {
            outputFilename = argv[++i];
        }
        else if (strcmp(s,  "-pts3d") == 0)
        {
            pts3dPath = argv[++i];
        }
        else if (strcmp(s,  "-pts2d") == 0)
        {
            pts2dPath = argv[++i];
        }
    }

    vector<Vec3d> pts3d;
    vector<int>   ids3d;
    readPointFile<3>(pts3dPath, pts3d, ids3d);

    vector<Vec2d> pts2d;
    vector<int>   ids2d;
    readPointFile<2>(pts2dPath, pts2d, ids2d);

    runAndSave(outputFilename, pts3d, ids3d, pts2d, ids2d, imageSize,
               boardSize, stepSize, squareSize, aspectRatio,
               flags, cameraMatrix, distCoeffs,
               writeExtrinsics, writePoints);

    return 0;
}