/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <cstdio>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include "../src/tinytag.h"

using namespace std;
using namespace cv;

double computeReprojectionErrors(const vector<vector<Point3f> > &objectPoints,
                                 const vector<vector<Point2f> > &imagePoints,
                                 const vector<Mat>              &rvecs,
                                 const vector<Mat>              &tvecs,
                                 const Mat                      &cameraMatrix,
                                 const Mat                      &distCoeffs,
                                 vector<float>                  &perViewErrors)
{
    vector<Point2f> imagePoints2;
    int             totalPoints = 0;
    double          totalErr    = 0, err;
    perViewErrors.resize(objectPoints.size());

    for (size_t i = 0; i < objectPoints.size(); i++)
    {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]),
                   Mat(imagePoints2),
                   NORM_L2);
        size_t n = objectPoints[i].size();
        perViewErrors[i] = static_cast<float>(std::sqrt(err * err / n));
        totalErr        += err * err;
        totalPoints     += static_cast<int>(n);
    }

    return std::sqrt(totalErr / totalPoints);
}

int main(int   argc,
         char *argv[])
{
    TinyTagDetector detector;

    if (argc != 5)
    {
        cerr << "USAGE:\n\tcalibrate_with_tags <#corners X> <#corners Y>" <<
            "<pattern_%d.png> <out.xml>\n" << endl;

        return -1;
    }

    string outPath = argv[4];

    Size   camSize;
    size_t width, height;
    if (!(istringstream(argv[1]) >> width))
    {
        cerr << "wrong number of X corners" << endl;
        exit(1);
    }
    if (!(istringstream(argv[2]) >> height))
    {
        cerr << "wrong number of Y corners" << endl;
        exit(1);
    }
    cout << width << " " << height << endl;

    vector< vector<Point2f> > imagePoints;
    vector< vector<Point3f> > objectPoints;
    vector<Point2f>           iPoints(width * height);
    vector<Point3f>           oPoints(width * height);
    for (size_t i = 0; i < width * height; ++i)
    {
        oPoints[i] = Point3d(i / width * 1.0, i % width * 1.0, 0.0);
    }

    vector<TinyTagInfo> tags;
    Mat                 image;
    Mat                 dImage;
    for (int i = 0;; ++i)
    {
        string path = format(argv[3], i);
        image = imread(path, -1);
        if (image.empty())
        {
            cerr << "could not load " << path << endl;
            break;
        }
        if (camSize == Size()) { camSize = image.size(); }
        image ^= Scalar::all(255);
        detector.detectTags(tags, image);
        cout << "Found " << tags.size() << " tags in image " << path << endl;
        dImage = drawTags(image, tags);

        // namedWindow("tags");
        // imshow("tags", dImage);
        // waitKey(0);

        int j = 0;

        vector<TinyTagInfo>::const_iterator itE = tags.end();
        for (vector<TinyTagInfo>::const_iterator it = tags.begin(); it != itE;
             ++it)
        {
            // cout << "Tag " << j << " center pos : [" << it->center.x << ","
            // << it->center.y << "] id : " << it->id << endl;
            iPoints[static_cast<size_t>(it->id() - 48)] = Point2f(it->center().x,
                                                                  it->center().y);
            // cout << it->id << " " << it->center.x << " " << it->center.y <<
            // endl;
            ++j;
        }

        imagePoints.push_back(iPoints);
        // cout << Mat(iPoints) << endl;
        // cout << Mat(oPoints) << endl;
    }
    objectPoints.resize(imagePoints.size(), oPoints);

    size_t      count = imagePoints.size();
    Mat         camera, distortions;
    vector<Mat> rvecs(count), tvecs(count);

    double        RMS;
    double        totalAvgErr;
    vector<float> reprojErrs;

    /*
     *   cout << "========================================" << endl;
     *   cout << "Base model" << endl;
     *   RMS = calibrateCamera(objectPoints, imagePoints, camSize, camera, distortions,
     *        rvecs, tvecs);
     *   cout << "RMS : " << RMS << endl;
     *
     *   totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
     *            rvecs, tvecs, camera, distortions, reprojErrs);
     *
     *   cout << "Per images : " << Mat(reprojErrs) << endl << " total " <<
     *    totalAvgErr << endl;
     *
     *   cout << camera << endl;
     *   cout << distortions << endl;
     *   cout << "========================================" << endl;
     */

    /*
     *   cout << "========================================" << endl;
     *   cout << "Rational model" << endl;
     *   RMS = calibrateCamera(objectPoints, imagePoints, camSize, camera, distortions,
     *        rvecs, tvecs, CV_CALIB_RATIONAL_MODEL);
     *   totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
     *            rvecs, tvecs, camera, distortions, reprojErrs);
     *
     *   cout << "RMS : " << RMS << endl;
     *   cout << "Per images : " << Mat(reprojErrs) << endl << " total " <<
     *    totalAvgErr << endl;
     *
     *   cout << camera << endl;
     *   cout << distortions << endl;
     *   cout << "========================================" << endl;
     */

    /*
     *   cout << "========================================" << endl;
     *   cout << "K1, K2, K3" << endl;
     *   RMS = calibrateCamera(objectPoints, imagePoints, camSize, camera, distortions,
     *        rvecs, tvecs, CV_CALIB_ZERO_TANGENT_DIST);
     *   totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
     *            rvecs, tvecs, camera, distortions, reprojErrs);
     *
     *   cout << "RMS : " << RMS << endl;
     *   cout << "Per images : " << Mat(reprojErrs) << endl << " total " <<
     *    totalAvgErr << endl;
     *
     *   cout << camera << endl;
     *   cout << distortions << endl;
     *   cout << "========================================" << endl;
     */

    cout << "========================================" << endl;
    cout << "K1, K2, P1, P2" << endl;
    RMS = calibrateCamera(objectPoints, imagePoints, camSize, camera,
                          distortions,
                          rvecs, tvecs, CALIB_FIX_K3);
    cout << "RMS : " << RMS << endl;

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                            rvecs, tvecs, camera, distortions,
                                            reprojErrs);

    cout << "Per images : " << Mat(reprojErrs) << endl << " total " <<
        totalAvgErr << endl;

    cout << camera << endl;
    cout << distortions << endl;
    cout << "========================================" << endl;

    /*
     *   cout << "========================================" << endl;
     *   cout << "K1, K2" << endl;
     *   RMS = calibrateCamera(objectPoints, imagePoints, camSize, camera, distortions,
     *        rvecs, tvecs, CV_CALIB_ZERO_TANGENT_DIST | CV_CALIB_FIX_K3);
     *   totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
     *            rvecs, tvecs, camera, distortions, reprojErrs);
     *
     *   cout << "RMS : " << RMS << endl;
     *   cout << "Per images : " << Mat(reprojErrs) << endl << " total " <<
     *    totalAvgErr << endl;
     *
     *   cout << camera << endl;
     *   cout << distortions << endl;
     *   cout << "========================================" << endl;
     */

    FileStorage fs(outPath, FileStorage::WRITE);
    fs << "K" << camera;
    fs << "coeffs" << distortions;

    return 0;
}
