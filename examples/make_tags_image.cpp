/*
 * License Agreement for TinyTag
 *
 * Copyright (c) 2012-2015, Nicolas Martin (nicolas.martin.3d@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <cstdio>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <xtclap/CmdLine.h>
#include <xtclap/PositiveConstraint.h>
#include <mvg/logger.h>
#include "../src/tinytag.h"

using namespace TCLAP;
using namespace std;
using namespace cv;

void addTags(TinyTagMaker &maker,
             Mat          &image,
             int           startId,
             int           size,
             int           step,
             int           n,
             int           nx,
             int           ny)
{
    Mat patt, tag;
    int width  = image.cols;
    int height = image.rows;

    int stepX = (width - nx * size) / (nx + 1);
    int stepY = (height - ny * size) / (ny + 1);
    step = std::min(stepX, stepY);

    int startX =
        static_cast<int>((width - (nx * size) - ((nx + 1) * step)) / 2.0);
    int startY =
        static_cast<int>((height - (ny * size) - ((ny + 1) * step)) / 2.0);

    for (int j = 0; j < ny; ++j)
    {
        int offY = startY + (j + 1) * step;

        for (int i = 0; i < nx; ++i)
        {
            int offX = startX + (i + 1) * step;

            int    y  = offY + j * size;
            int    x  = offX + i * size;
            size_t id = static_cast<size_t>(startId + j * nx + i);

            /*
             *   logInfo() << x << x+size << image.cols;
             *   logInfo() << y << y+size << image.rows;
             */

            maker.makeTag(tag, id, static_cast<size_t>(size / (n + 2)));
            patt = image(Range(y, y + size), Range(x, x + size));
            tag.copyTo(patt);
        }
    }
}

int main(int   argc,
         char *argv[])
{
    CmdLine cmd("Creates an image of tag that can be printed or projected for "
                "fiducial marker detection. For an image to be generated, the following "
                "criterias concerning the parameters must be met : "
                "nx*size + (nx+1)*step < width and "
                "ny*size + (ny+1)*step < height and "
                "size%(n+2) == 0.");

    ValueArg<int> widthArg("W", "width",
                           "Width of the constructed tag image. The actual width of the "
                           "generated image might be bigger if magnification is not 1.",
                           false, 800, new PositiveConstraint<int>(),
                           cmd);

    ValueArg<int> heightArg("H", "height",
                            "Height of the constructed tag image. The actual height of the "
                            "generated image might be bigger if magnification is not 1.",
                            false, 600,
                            new PositiveConstraint<int>(),
                            cmd);

    ValueArg<int> sizeArg("s", "tag-size",
                          "Size of the tag in pixels.",
                          false, -1, new PositiveConstraint<int>(),
                          cmd);

    ValueArg<int> stepArg("t", "step",
                          "Step between tags in pixels. If not provided, it will be computed as "
                          "the largest value that meet the criterias. If given, nx and ny params "
                          "are ignored.",
                          false, -1,
                          new PositiveConstraint<int>(),
                          cmd);

    ValueArg<int> nxArg("x", "num-tags-x",
                        "Number of tags in the horizontal direction. If not provided, it will be "
                        "computed as the largest value that meet the criterias. If given, step "
                        "param is ignored.",
                        false, 8,
                        new PositiveConstraint<int>(),
                        cmd);

    ValueArg<int> nyArg("y", "num-tags-y",
                        "Number of tags in the vertical direction. If not provided, it will be "
                        "computed as the largest value that meet the criterias. If given, step "
                        "param is ignored.",
                        false, 6,
                        new PositiveConstraint<int>(),
                        cmd);

    ValueArg<int> magnifyArg("m", "magnify",
                             "Image magnification (scale). The final image will have dimension "
                             "(width*magnify) x (height*magnify).",
                             false, 1,
                             new PositiveConstraint<int>(),
                             cmd);

    SwitchArg negateArg("i", "inverse-color",
                        "Inverse color (for printing or displaying purposes).",
                        cmd, false);

    ValueArg<int> nArg("n", "n-bits",
                       "This is the n used when defining the tag maker.\n"
                       "Tags will be of size (n+2) x (n+2) with 1 pixel border.",
                       false, 5, new PositiveConstraint<int>(),
                       cmd);

    ValueArg<int> kArg("k", "k-bits",
                       "This is the number of bits used to store information in the n x n bits.\n"
                       "This means n x n - k bits are used for redundancy.",
                       false, 9,
                       new PositiveConstraint<int>(),
                       cmd);

    ValueArg<int> startIdArg("d", "start-id",
                             "Id of the tag to start the generation. Each of the nx x ny tags will "
                             "have id start-id, start-id+1, start-id+2, ...",
                             false, 0,
                             new PositiveConstraint<int>(),
                             cmd);

    ValueArg<int> colorArg("c", "color",
                           "Color that will be used to replace white pixels in the generated image. "
                           "This is especially useful when printing, to be able to detect tags while "
                           "allowing overlay projections.",
                           false, 255, "0 <= color <= 255",
                           cmd);

    ValueArg<string> outArg("o", "out-path",
                            "Path of the generated image to save.",
                            false, "tags.png", "string",
                            cmd);

    cmd.parse(argc, argv);

    int    width   = widthArg.getValue();
    int    height  = heightArg.getValue();
    int    size    = sizeArg.getValue();
    int    step    = stepArg.getValue();
    int    nx      = nxArg.getValue();
    int    ny      = nyArg.getValue();
    int    n       = nArg.getValue();
    int    k       = kArg.getValue();
    int    magnify = magnifyArg.getValue();
    bool   negate  = negateArg.getValue();
    int    startId = startIdArg.getValue();
    int    color   = colorArg.getValue();
    string out     = outArg.getValue();

    if (size < 0)
    {
        size = (n + 2) * 4;
    }

    if (size % (n + 2) != 0)
    {
        logError() << "Tag size must be a multiple of " << n + 2;
    }

    if ((color < 0) || (color > 255))
    {
        logError() << "Color must be >= 0 and <= 255";
    }

    TinyTagMaker maker(static_cast<size_t>(n), static_cast<size_t>(k));

    if (step < 0)
    {
        int sx = (width - nx * size) / (nx + 1);
        int sy = (height - ny * size) / (ny + 1);
        step = std::min(sx, sy);
        /*
         *   logInfo() << sx << nx*size+(nx+1)*sx << nx*size+(nx+1)*step << width;
         *   logInfo() << sy << ny*size+(ny+1)*sy << ny*size+(ny+1)*step << height;
         */
        if (step < 0)
        {
            logError() << "Size of tag is too big to fit " << nx << "x" << ny <<
                " tags in the image";
        }
        if (step < 10)
        {
            logWarning() <<
                "Step is very small between tags, detection might not work ...";
        }
    }
    else
    {
        if (step < 10)
        {
            logWarning() <<
                "Step is very small between tags, detection might not work ...";
        }
        nx = (width - step) / (step + size);
        ny = (height - step) / (step + size);
        if ((nx < 0) || (ny < 0))
        {
            logError() << "Step or size is too big to fit " << nx << "x" <<
                ny << " tags in the image";
        }
    }

    Mat pImage(height, width, CV_8U, Scalar::all(0));
    addTags(maker, pImage, startId, size, step, n, nx, ny);

    Mat fImage;
    if (magnify != 1)
    {
        resize(pImage, fImage, Size(), magnify, magnify, INTER_NEAREST);
    }
    else
    {
        fImage = pImage.clone();
    }

    if (negate)
    {
        fImage ^= Scalar::all(255);
    }

    MatIterator_<uchar> it = fImage.begin<uchar>();
    while (it != fImage.end<uchar>())
    {
        if (negate && (*it == 0))
        {
            *it = static_cast<uchar>(color);
        }
        else if (!negate && (*it == 0xff))
        {
            *it = static_cast<uchar>(color);
        }
        ++it;
    }

    vector<TinyTagInfo> tags;
    Mat                 dImage;

    TinyTagDetector detector(static_cast<size_t>(n), static_cast<size_t>(k));
    if (!detector.sanityCheck())
    {
        return 1;
    }

    imwrite(out, fImage);
    detector.detectTags(tags, pImage);
    cout << "Detected " << tags.size() << " tags" << endl;
    dImage = drawTags(pImage, tags);

    namedWindow("tags", WINDOW_NORMAL);
    imshow("tags", dImage);
    waitKey(0);
}
