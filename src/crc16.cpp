/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crc16.h"

/*
 * See
 * http://stackoverflow.com/questions/22432066/how-to-use-table-based-crc-16-code
 * for an example of implementation on which this one is loosely based
 */
struct CRC16::Private
{
    Private(unsigned long poly) : m_poly(poly & 0xffff)
    {
        makeCRCTable();
    }

    void makeCRCTable()
    {
        unsigned long c;

        for (unsigned long i = 0; i < 256; i++)
        {
            c = i << 8;
            for (unsigned long j = 0; j < 8; j++)
            {
                c = (c & 0x8000) ? m_poly ^ (c << 1) : (c << 1);
            }
            m_crcTable[i] = static_cast<unsigned short>(c);
        }
    }

    unsigned short checksum(unsigned long value)
    {
        unsigned short crc = INIT_VAL;
        unsigned char *p   = reinterpret_cast<unsigned char *>(&value);

        for (int i = 3; i >= 0; --i)
        {
            unsigned short v = (crc >> 8) ^ p[i];
            crc = static_cast<unsigned short>((crc << 8) ^ m_crcTable[v]);
        }

        return crc ^ FINISH_VAL;
    }

    unsigned short              m_crcTable[256];
    unsigned long               m_poly;
    static const unsigned short INIT_VAL   = 0xffff;
    static const unsigned short FINISH_VAL = 0x0000;
};

CRC16::CRC16(unsigned long poly)
{
    m_d = new Private(poly);
}

CRC16::CRC16(const CRC16 &i) :
    m_d(new Private(*i.m_d))
{ }

CRC16 & CRC16::operator=(const CRC16 &i)
{
    if (this != &i)
    {
        delete m_d;
        m_d = new Private(*i.m_d);
    }

    return *this;
}

CRC16::~CRC16()
{
    delete m_d;
}

unsigned short CRC16::checksum(unsigned long value)
{
    return m_d->checksum(value);
}
