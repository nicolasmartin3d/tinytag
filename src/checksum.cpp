/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "crc16.h"
#include "checksum.h"

#include <mvg/logger.h>
#if defined FUNCTIONAL_IN_STD_HEADER
#include <functional>
#else
#include <tr1/functional>
#endif

struct CheckSum::Private
{
    struct Impl
    {
        Impl() { }
        virtual ~Impl();
        virtual size_t compute(size_t value) = 0;

        private:
            Impl(const Impl &) { }
            void operator=(const Impl &) { }
    };

    // for backward compatibility
    struct CRC16Impl : public Impl
    {
        virtual size_t compute(size_t value);
        CRC16 crc;
    };

    struct GenericImpl : public Impl
    {
#ifdef ARCH_64
        GenericImpl(size_t nbBits_=64) : nbBits(nbBits_) { }
#else
        GenericImpl(size_t nbBits_=32) : nbBits(nbBits_) { }
#endif
        virtual size_t compute(size_t value);
        size_t nbBits;
    };

    Private(size_t n)
    {
        if (n == 16) { m_impl = new CRC16Impl; }
        else  m_impl = new GenericImpl;
    }

    ~Private()
    {
        delete m_impl;
    }

    Impl *m_impl;
};

CheckSum::Private::Impl::~Impl()
{ }

size_t CheckSum::Private::CRC16Impl::compute(size_t value)
{
    return static_cast<size_t>(crc.checksum(static_cast<unsigned long>(value)));
}

size_t CheckSum::Private::GenericImpl::compute(size_t value)
{
    size_t x = value;

    if (nbBits == 64)
    {
#ifdef ARCH_64
        return ((((x ^ 0x1851b2a3dbb7d9f6) + (x << 13 | x >> 19)) *
                 0x37c25df138b9e5a7) ^ 0xefb2bdacb69f08f5) *
               0xa824494b766c7c09;
#else
        logError() << "64 bits checksum asked on a 32 bits architecture.";
#endif
    }

    return ((((x ^ 0x1851b2a3) + (x << 13 | x >> 19)) * 0x37c25df1) ^
            0xefb2bdac) * 0xa824494c;
}

CheckSum::CheckSum(size_t n)
{
    m_d = new Private(n);
}

CheckSum::CheckSum(const CheckSum &s) :
    m_d(new Private(*s.m_d))
{ }

CheckSum & CheckSum::operator=(const CheckSum &s)
{
    if (this != &s)
    {
        delete m_d;
        m_d = new Private(*s.m_d);
    }

    return *this;
}

CheckSum::~CheckSum()
{
    delete m_d;
}

size_t CheckSum::compute(size_t value)
{
    return m_d->m_impl->compute(value);
}
