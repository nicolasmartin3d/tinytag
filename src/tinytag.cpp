/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <list>
#include <map>
#include "canny.h"
#include "checksum.h"
#include "tinytag.h"

using namespace std;
using namespace cv;

#define CV_SHIFT (1 << 16)

void rot90(cv::Mat &matImage,
           int      rotflag)
{
    if (rotflag == 2)
    {
        transpose(matImage, matImage);
        flip(matImage, matImage, 1);
    }
    else if (rotflag == 0)
    {
        transpose(matImage, matImage);
        flip(matImage, matImage, 0);
    }
    else if (rotflag == 3)
    {
        flip(matImage, matImage, -1);
    }
    else if (rotflag != 1)    // if not 0,1,2,3:
    {
        cout << "Unknown rotation flag(" << rotflag << ")" << endl;
    }
}

struct TinyTagInfo::Private
{
    Private(Point2f              center_,
            std::vector<Point2f> corners_,
            int                  id_);
    ~Private();

    Point2f              center;
    std::vector<Point2f> corners;
    int                  id;
};

TinyTagInfo::Private::Private(Point2f              center_,
                              std::vector<Point2f> corners_,
                              int                  id_) :
    center(center_),
    corners(corners_),
    id(id_)
{ }

TinyTagInfo::Private::~Private()
{ }

TinyTagInfo::TinyTagInfo(Point2f              center_,
                         std::vector<Point2f> corners_,
                         int                  id_)
{
    m_privImpl = new Private(center_, corners_, id_);
}

TinyTagInfo::TinyTagInfo(const TinyTagInfo &i) :
    m_privImpl(new Private(*i.m_privImpl))
{ }

TinyTagInfo & TinyTagInfo::operator=(const TinyTagInfo &i)
{
    if (this != &i)
    {
        delete m_privImpl;
        m_privImpl = new Private(*i.m_privImpl);
    }

    return *this;
}

TinyTagInfo::~TinyTagInfo()
{
    delete m_privImpl;
}

double TinyTagInfo::approxArea() const
{
    return cv::contourArea(m_privImpl->corners);
}

cv::Point2f TinyTagInfo::center() const
{
    return m_privImpl->center;
}

const std::vector<cv::Point2f> &TinyTagInfo::corners() const
{
    return m_privImpl->corners;
}

int TinyTagInfo::id() const
{
    return m_privImpl->id;
}

void make_permutations(RNG             rng,
                       size_t          n,
                       size_t          k,
                       vector<size_t> &perm,
                       vector<size_t> &uperm)
{
    perm.resize(n * n);
    uperm.resize(n * n);

    // This is for backward compatibility for when the RNG was not cross
    // platform and we had to hard-code the permutations. This seed is a special
    // value that puts us in compatibility mode. If any other seed is used, we
    // are guaranteed generate the same random values cross platform, so there
    // is no need for such a test.
    if ((n == 5) && (k == 9) && (rng.state == 0x27d2))
    {
        unsigned char _perm[] = {
            7, 19, 21, 0, 14, 6, 20, 10, 16, 2, 8, 15, 17, 9, 5, 18, 24, 11, 1,
            4, 3, 13, 22, 23, 12
        };
        copy(_perm, _perm + 25, perm.begin());
    }
    else
    {
        for (size_t i = 0; i < n * n; ++i)
        {
            perm[i] = static_cast<unsigned char>(i);
        }

        // we use our own loop that uses the RNG not C++ random_shuffles
        for (size_t i = 0; i < 2 * n * n; ++i)
        {
            size_t a = rng() % (n * n);
            size_t b = rng() % (n * n);
            swap(perm[a], perm[b]);
        }
    }

    for (size_t i = 0; i < n * n; ++i)
    {
        uperm[perm[i]] = static_cast<unsigned char>(i);
    }
}

/**************************************************************************
* Detector : detects and identify tags in an image
**************************************************************************/
struct TinyTagDetector::Private
{
    Private(size_t        n,
            size_t        k,
            unsigned long seed) :
        sumChecker(n * n - k),
        rng(seed),
        m_seed(seed),
        m_n(n),
        m_k(k)
    {
        m_z = m_n * m_n - m_k;
        make_permutations(rng, n, k, perm, uperm);
    }

    /*
     *   int identifyTag(const Mat &src)
     *   {
     *    size_t res = 0;
     *    for (int j = m_n-1; j >= 0; --j)
     *    {
     *        const unsigned char *p = src.ptr<unsigned char>(j);
     *        for (int i = m_n-1; i >= 0; --i)
     *            res = (res << 1) | (p[i] & 1);
     *    }
     *
     *    //cout << "identify" << endl;
     *    //cout << "res : " <<  res << endl;
     *
     *    size_t rec = 0;
     *    for (int i = 0; i < m_n*m_n; ++i)
     *        rec |= (((res >> i) & 1ULL) << uperm[i]);
     *    //cout << "rec : " <<  rec << endl;
     *
     *    size_t maskZ = (1ULL << m_z) - 1; //careful for overflow .. we must check this
     *    size_t rcsum = rec & maskZ;
     *    //cout << "maskZ : " << maskZ << " rcsum " << rcsum << endl;
     *
     *    size_t maskK = (1ULL << m_k) - 1; //careful for overflow .. we must check this
     *    size_t rval  = (rec >> m_z) & maskK;
     *    //cout << "maskK : " << maskK << " rval " << rval << endl;
     *
     *    size_t csum = sumChecker.compute(rval) & maskZ;
     *
     *    return csum == rcsum ? rval : -1;
     *   }
     */

    int identifyTag(const cv::Mat &src,
                    int           &order)
    {
        vector<unsigned char>            buf(m_n * m_n);
        MatConstIterator_<unsigned char> it = src.begin<unsigned char>();
        copy(it, it + static_cast<ptrdiff_t>(m_n * m_n), buf.begin());

        for (int rot = 0; rot < 4; ++rot)
        {
            size_t res = 0;
            switch (rot)
            {
                case 1: {
                    for (size_t j = m_n; j > 0; --j)
                    {
                        for (size_t i = m_n; i > 0; --i)
                        {
                            res = (res << 1) | (buf[(j - 1) * m_n + (i - 1)] & 1);
                        }
                    }
                    break;
                }
                case 2: {
                    for (size_t i = 0; i < m_n; ++i)
                    {
                        for (size_t j = m_n; j > 0; --j)
                        {
                            res = (res << 1) | (buf[(j - 1) * m_n + i] & 1);
                        }
                    }
                    break;
                }
                case 3: {
                    for (size_t j = 0; j < m_n; ++j)
                    {
                        for (size_t i = 0; i < m_n; ++i)
                        {
                            res = (res << 1) | (buf[j * m_n + i] & 1);
                        }
                    }
                    break;
                }
                case 0: {
                    for (size_t i = m_n; i > 0; --i)
                    {
                        for (size_t j = 0; j < m_n; ++j)
                        {
                            res = (res << 1) | (buf[j * m_n + (i - 1)] & 1);
                        }
                    }
                    break;
                }
            }

            size_t rec = 0;
            for (size_t i = 0; i < m_n * m_n; ++i)
            {
                rec |= (((res >> i) & 1) << uperm[i]);
            }

            size_t maskZ = (1 << m_z) - 1; // careful for overflow .. we must
                                           // check this
            size_t rcsum = rec & maskZ;

            size_t maskK = (1 << m_k) - 1; // careful for overflow .. we must
                                           // check this
            size_t rval = (rec >> m_z) & maskK;

            size_t csum = sumChecker.compute(rval) & maskZ;

            if (csum == rcsum)
            {
                order = rot;

                return static_cast<int>(rval);
            }
        }

        return -1;
    }

    void detectTags(vector<TinyTagInfo> &tags,
                    const Mat           &src)
    {
        bool debugimage = false;
        // bool debugimage = true;

        gray0.create(src.size(), CV_8U);
        // const int minSize = 10;
        const int minLength = 30;

        if (src.channels() == 3) { cvtColor(src, gray0, COLOR_BGR2GRAY); }
        else { src.copyTo(gray0); }

        cannyAuto(gray0, gray, 0.9);
        // cv::Canny(gray0, gray, 5, 13);
        // Canny(gray0, gray, 20, 120);
        // Canny(gray0, gray, meanVal*0.66, meanVal*1.33);

        if (debugimage)
        {
            imshow("canny", gray);
            waitKey(10);
        }

        vector<vector<Point> > contours;
        vector<bool>           kept;
        vector< Vec4i >        hierarchy;
        // should use CV_RETR_EXTERNAL, especially if tags are printed and the
        // contour of the sheet is detected ..
        // findContours(gray, contours, hierarchy, CV_RETR_EXTERNAL,
        // CV_CHAIN_APPROX_SIMPLE);
        findContours(gray, contours, hierarchy, RETR_CCOMP,
                     CHAIN_APPROX_SIMPLE);

        kept.resize(contours.size(), true);

        map<int, list<TinyTagInfo> > tagsIndexed;

        vector<Point> approx, hull;
        Mat           homog, fhomog, tag, tagt, tmp, rot;
        Point2f       qs[] = {
            Point2f(-0.5f, -0.5f),
            Point2f(m_n + 1.5f, -0.5f),
            Point2f(m_n + 1.5f, m_n + 1.5f),
            Point2f(-0.5f, m_n + 1.5f)
        };
        Point2f       ps[4];
        double        _H[] = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
        Mat           H(3, 3, CV_64F, _H);

        //    namedWindow("tag");
        //    namedWindow("rtag");
        Mat image;

        // for( size_t i = 0; i < contours.size(); i++ ) {
        // printf("child : %d parent : %d\n", hierarchy[i][2], hierarchy[i][3]);
        // }
        // getchar();

        // test each contour
        for (size_t i = 0; i < contours.size(); i++)
        {
            if (!kept[i])
            {
                // any neighboors or children of a refused tag can not be a real
                // tag
                // 0 : next 1 : previous 2 : children 3 : parent
                int v = hierarchy[i][0];
                while (v != -1)
                {
                    kept[static_cast<size_t>(v)] = false; v =
                        hierarchy[static_cast<size_t>(v)][0];
                }
                v = hierarchy[i][1];
                while (v != -1)
                {
                    kept[static_cast<size_t>(v)] = false; v =
                        hierarchy[static_cast<size_t>(v)][1];
                }
                // v = hierarchy[i][2];
                // while (v != -1) { kept[v] = false; v = hierarchy[v][2]; }
                continue;
            }

            // any contour enclosed can not be a real tag, except if the contour
            // is big enough ...
            if ((hierarchy[i][2] != -1) &&
                (arcLength(Mat(contours[static_cast<size_t>(hierarchy[i][2])]),
                           false) < 10 * minLength))
            {
                // v = hierarchy[i][2];
                // while (v != -1) { kept[v] = false; v = hierarchy[v][2]; }
                kept[static_cast<size_t>(hierarchy[i][2])] = false;
            }

            double len = arcLength(Mat(contours[i]), false);
            if (len < minLength) { continue; }

            /*
             *   convexHull(Mat(contours[i]), hull, false);
             *   if (debugimage) {
             *   src.copyTo(image);
             *   const Point *p = &hull[0];
             *   int n = hull.size();
             *   polylines(image, &p, &n, 1, true, Scalar(random()%255, random()%255, random()%255),
             * 1, CV_AA);
             *
             *   imshow("image", image); waitKey(0);
             *   //t = tmin = tmax = 0.086;
             *   }
             */

            /*
             *   if (debugimage) {
             *   src.copyTo(image);
             *   const Point *p = &approx[0];
             *   int n = approx.size();
             *   polylines(image, &p, &n, 1, true, Scalar(random()%255, random()%255, random()%255),
             * 1, CV_AA);
             *   imshow("image", image); waitKey(0);
             *   }
             */

            for (int k = 0; k < 2; ++k)
            {
                int    it   = 0;
                double tmin = 0.01, tmax = 0.5;
                while (it < 20 && tmin <= tmax)
                {
                    double t = (tmin + tmax) / 2;
                    // approxPolyDP(Mat(contours[i]), approx, max(len*t, 2.),
                    // true);
                    approxPolyDP(Mat(contours[i]), approx,
                                 max(len * t, 0.5), true);

                    /*
                     *   double len = arcLength(Mat(approx), false);
                     *   printf("\t len = %f\n", len);
                     *
                     *   RotatedRect r = minAreaRect(Mat(contours[i]));
                     *   if (fabs(r.size.area()) < minSize*minSize) continue;
                     *   if (r.size.width < minSize) continue;
                     *   if (r.size.height < minSize) continue;
                     *
                     *   printf("\tr : %f %f %f\n", fabs(r.size.area()), r.size.width,
                     *    r.size.height);
                     */

                    if (debugimage)
                    {
                        src.copyTo(image);
                        const Point *p = &approx[0];
                        int          n = static_cast<int>(approx.size());
#if CV_MAJOR_VERSION >= 3
                        polylines(image, &p, &n, 1, true,
                                  Scalar(rng() % 255, rng() % 255,
                                         rng() % 255), 1, LINE_AA);
#else
                        polylines(image, &p, &n, 1, true,
                                  Scalar(rng() % 255, rng() % 255,
                                         rng() % 255), 1, CV_AA);
#endif

                        /*
                         *   Point2f vertices[4];
                         *   r.points(vertices);
                         *   for (int i = 0; i < 4; i++)
                         *   line(image, vertices[i], vertices[(i+1)%4], Scalar(0,255,0));
                         */

                        cout << t << " " << tmin << " " << tmax << endl;
                        imshow("image", image); waitKey(0);
                        // t = tmin = tmax = 0.086;
                    }
                    size_t size = approx.size();
                    if (size == 4) { break; }
                    else if (size > 4)
                    {
                        tmin = t;
                    }
                    else { tmax = t; }
                    it++;
                }
                if (approx.size() == 4)
                {
                    break;
                }

                // some contours are self intersecting ... try to remove the
                // holes by "convexing it"
                convexHull(Mat(contours[i]), hull, false);
                contours[i].resize(hull.size());
                copy(hull.begin(), hull.end(), contours[i].begin());
            }

            if (debugimage)
            {
                if (approx.size() == 4)
                {
                    cout << "\tPotential square found : area ("
                         << fabs(contourArea(Mat(approx))) << ") "
                         << " convex ? "
                         << (isContourConvex(Mat(approx)) ? "Y" : "N")
                         << endl;
                }
            }

            if ((approx.size() == 4) &&
                // fabs(contourArea(Mat(approx))) > 200 &&
                // fabs(contourArea(Mat(approx))) > 150 &&
                isContourConvex(Mat(approx))
                )
            {
                if (debugimage)
                {
                    cout << "\tSquare is convex and of sufficient size" << endl;
                }
                /*
                 *   cout << "length : " << arcLength(Mat(contours[i]), false) << " rect : [" <<
                 * r.size.width << " " << r.size.height << "] " << " area : " << fabs(r.size.area())
                 * <<  " contour area : " << fabs(contourArea(Mat(approx))) << endl;
                 */

                // Just to order the points !
                convexHull(Mat(approx), hull, false);

                vector<Point2f> corners;
                size_t          hsize = hull.size();
                for (size_t j = 0; j < hsize; ++j)
                {
                    corners.push_back(Point2f(static_cast<float>(hull[j].x),
                                              static_cast<float>(hull[j].y)));
                }

                cornerSubPix(gray0, corners,
                             Size(3, 3),
                             Size(-1, -1),
                             TermCriteria(TermCriteria::COUNT |
                                          TermCriteria::EPS,
                                          50, 0.01));

                for (size_t j = 0; j < 4; ++j)
                {
                    ps[j] = corners[j];
                }

                homog  = getPerspectiveTransform(ps, qs);
                fhomog = homog.inv() * H;

                warpPerspective(gray0, tag, fhomog,
                                Size(static_cast<int>(m_n + 2),
                                     static_cast<int>(m_n + 2)),
                                WARP_INVERSE_MAP);

                //            double ts[] = {128, 123, 133, 118, 138, 113, 143,
                // 108,
                // 148, 103, 153, 98, 158, 93, 163, 88, 168, 83, 173, 78, 178,
                // 73,
                // 183, 68, 188, 63, 193, 58, 198, 53, 203, 48, 208, 43, 213,
                // 38,
                // 218, 33, 223, 28, 228, 23, 233, 18, 238, 13, 243, 8, 248, 3,
                // 253};
                double ts[] = {
                    0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, -5, -10, -15, -20,
                    -25, -30, -35, -40, -45, -50, -55, 55, -60, 60, -65, 65,
                    -70, 70, -75, 75, -80, 80, -85, 85, -90, 90, -95, 95, -100,
                    100
                };
                // bool found = false;
                // does not cost that much to iter 50 times in this order ...
                for (int k = 0; k < 40 - 18; ++k)
                {
                    adaptiveThreshold(tag, tagt, 255,
                                      ADAPTIVE_THRESH_GAUSSIAN_C,
                                      THRESH_BINARY, 5, ts[k]);
                    // threshold(tag, tagt, ts[k], 255, THRESH_BINARY);

                    if (debugimage)
                    {
                        cout << k << " t = " << ts[k] << endl;
                        imshow("rtag", tagt);
                        //                    imshow("rtmp", tmp);
                        imshow("tag", tag);
                        waitKey(0);
                    }

                    /*
                     *   // test each orientation
                     *   for (int r = 0; r < 4; ++r)
                     *   {
                     *   //                    rot = getRotationMatrix2D(
                     *   // Point2f(7,7), 90*r, 1.);
                     *   rot = getRotationMatrix2D(Point2f(3, 3), 90 * r, 1.);
                     *   //                    warpAffine(tagt, tmp, rot,
                     *   // Size(7+8,7+8), INTER_LINEAR);
                     *   warpAffine(tagt, tmp, rot, Size(7, 7), INTER_LINEAR);
                     *   //                    int id =
                     *   // identifyTag(tmp(Range(5,5+5),Range(5,5+5)));
                     *   tmp = tagt;
                     *   rot90(tmp, r);
                     *
                     *   }
                     */

                    int r  = -1;
                    int id = identifyTag(
                        tagt(Range(1, static_cast<int>(1 + m_n)),
                             Range(1, static_cast<int>(1 + m_n))), r);
                    if (id != -1)
                    {
                        vector<Point2f> tcorners(corners.size());
                        copy(corners.begin(), corners.end(),
                             tcorners.begin());

                        // re-align so that first match upper left corner
                        rotate(tcorners.begin(),
                               tcorners.begin() + r, tcorners.end());

                        double _q[3] = { m_n / 2.0 + 0.5, m_n / 2.0 + 0.5, 1 };
                        Mat    q(3, 1, CV_64F, _q);
                        q = homog.inv() * q;
                        /*
                         *   tags.push_back( Tag( Point2f(_q[0]/_q[2], _q[1]/_q[2]),
                         *                     tcorners, id) );
                         */
                        tagsIndexed[id].push_back(
                            TinyTagInfo(
                                Point2f(static_cast<float>(_q[0] / _q[2]),
                                        static_cast<float>(_q[1] / _q[2])),
                                tcorners,
                                id)
                            );
                        // found = true;

                        if (debugimage)
                        {
                            cout << "id " << id << " found with C = " <<
                                ts[k] << endl;
                        }
                        break;
                    }

                    /*
                     *   if (!found && k==4) {
                     *   namedWindow("tag");
                     *   imshow("tag", tmp);
                     *   waitKey(0);
                     *   }
                     */
                }
            }
        }

        tags.clear();
        for (map<int, list<TinyTagInfo> >::const_iterator it =
                 tagsIndexed.begin(), itE = tagsIndexed.end();
             it != itE; ++it)
        {
            TinyTagInfo bestTag = it->second.front();
            double      maxArea = bestTag.approxArea();
            for (list<TinyTagInfo>::const_iterator it2 = it->second.begin(),
                 it2E = it->second.end();
                 it2 != it2E; ++it2)
            {
                double area = it2->approxArea();
                if (area > maxArea)
                {
                    maxArea = area;
                    bestTag = *it2;
                }
            }

            tags.push_back(bestTag);
        }
    }

    vector<TinyTagInfo> detectTags(const Mat &src)
    {
        vector<TinyTagInfo> tags;
        detectTags(tags, src);

        return tags;
    }

    CheckSum            sumChecker;
    std::vector<size_t> perm, uperm;
    cv::Mat             gray0, gray, canny;
    cv::RNG             rng;
    unsigned long       m_seed;
    size_t              m_n;
    size_t              m_k;
    size_t              m_z;
};

TinyTagDetector::TinyTagDetector(size_t        n,
                                 size_t        k,
                                 unsigned long seed)
{
    m_d = new Private(n, k, seed);
}

TinyTagDetector::TinyTagDetector(const TinyTagDetector &i) :
    m_d(new Private(*i.m_d))
{ }

TinyTagDetector & TinyTagDetector::operator=(const TinyTagDetector &i)
{
    if (this != &i)
    {
        delete m_d;
        m_d = new Private(*i.m_d);
    }

    return *this;
}

TinyTagDetector::~TinyTagDetector()
{
    delete m_d;
}

/*
 *   int TinyTagDetector::identifyTag(const Mat &src)
 *   {
 *    return m_d->identifyTag(src);
 *   }
 */

int TinyTagDetector::identifyTag(const Mat &src,
                                 int       &order)
{
    return m_d->identifyTag(src, order);
}

void TinyTagDetector::detectTags(vector<TinyTagInfo> &tags,
                                 const Mat           &src)
{
    m_d->detectTags(tags, src);
}

bool TinyTagDetector::sanityCheck()
{
    cout << "n = " << m_d->m_n << endl;
    cout << "k = " << m_d->m_k << endl;
    cout << "z = " << m_d->m_z << endl;

    /*
     *   for (int i=0; i<m_d->m_n*m_d->m_n; ++i) {
     *    cout << int(m_d->perm[i]) << " ";
     *   }
     *   cout << endl;
     *
     *   for (int i=0; i<m_d->m_n*m_d->m_n; ++i) {
     *    cout << int(m_d->uperm[i]) << " ";
     *   }
     *   cout << endl;
     */

    TinyTagMaker maker(m_d->m_n, m_d->m_k, m_d->m_seed);
    for (size_t i = 0; i < (1ULL << m_d->m_k); ++i)
    {
        Mat tag = maker.makeTag(i);
        if (tag.size() != Size(static_cast<int>(m_d->m_n + 2),
                               static_cast<int>(m_d->m_n + 2)))
        {
            cerr << "Tags are of the wrong size "
                 << tag.size() << " vs " << Size(static_cast<int>(m_d->m_n + 2),
                                            static_cast<int>(m_d->m_n + 2))
                 << endl;

            return false;
        }

        for (int r = 0; r < 4; ++r)
        {
            Mat tmp = tag.clone();
            rot90(tmp, r);

            int k;
            int j = identifyTag(
                tmp(Range(1, static_cast<int>(1 + m_d->m_n)),
                    Range(1, static_cast<int>(1 + m_d->m_n))), k);
            if (i != static_cast<size_t>(j))
            {
                cerr << "Wrong id detected : " << j << " vs " << i << endl;

                return false;
            }
            if (k != r)
            {
                cerr << "For id " << i << " wrong rotation : " << k << " vs " <<
                    r << endl;

                return false;
            }
        }
    }

    return true;
}

std::vector<TinyTagInfo> TinyTagDetector::detectTags(const Mat &src)
{
    return m_d->detectTags(src);
}

/**************************************************************************
* Maker : constructs an image representation of a tag
**************************************************************************/

struct TinyTagMaker::Private
{
    Private(size_t        n,
            size_t        k,
            unsigned long seed) :
        sumChecker(n * n - k),
        rng(seed),
        m_seed(seed),
        m_n(n),
        m_k(k)
    {
        m_z = m_n * m_n - m_k;
        make_permutations(rng, n, k, perm, uperm);
    }

    void makeTag(Mat   &dst,
                 size_t id,
                 size_t magnification)
    {
        dst.create(static_cast<int>(m_n), static_cast<int>(m_n), CV_8U);

        // cout << "makeTag" << endl;

        /*
         *   for (int i=0; i<m_n*m_n; ++i) {
         *    cout << int(perm[i]) << " ";
         *   }
         *   cout << endl;
         *
         *   for (int i=0; i<m_n*m_n; ++i) {
         *    cout << int(uperm[i]) << " ";
         *   }
         *   cout << endl;
         */

        size_t maskK = (1ULL << m_k) - 1; // careful for overflow .. we must
                                          // check this
        size_t val = id & maskK;
        // cout << "maskK : " << maskK << " val " << val << endl;

        size_t maskZ = (1ULL << m_z) - 1; // careful for overflow .. we must
                                          // check this
        size_t csum = sumChecker.compute(val) & maskZ;
        // cout << "maskZ : " << maskZ << " csum " << csum << endl;

        size_t vec = (val << m_z) | csum;
        // cout << "rec : " << vec << endl;

        size_t res = 0;
        for (size_t i = 0; i < m_n * m_n; ++i)
        {
            res |= (((vec >> i) & 1ULL) << perm[i]);
        }

        // cout << "res = " << res << endl;

        for (size_t i = 0; i < m_n * m_n; ++i)
        {
            dst.at<unsigned char>(static_cast<int>(i / m_n),
                                  static_cast<int>(i % m_n)) =
                ((res >> i) & 1) * 255;
        }

        copyMakeBorder(dst, tmp, 1, 1, 1, 1, BORDER_CONSTANT, Scalar::all(255));

        if (magnification == 1) { tmp.copyTo(dst); }
        else
        {
            dst = Mat(static_cast<int>((m_n + 2) * magnification),
                      static_cast<int>((m_n + 2) * magnification), CV_8U);
            for (size_t y = 0; y < m_n + 2; ++y)
            {
                for (size_t x = 0; x < m_n + 2; ++x)
                {
                    for (size_t yy = 0; yy < magnification; ++yy)
                    {
                        for (size_t xx = 0; xx < magnification; ++xx)
                        {
                            dst.at<unsigned char>(
                                static_cast<int>(y * magnification + yy),
                                static_cast<int>(x * magnification + xx)) =
                                tmp.at<unsigned char>(static_cast<int>(y),
                                                      static_cast<int>(x));
                        }
                    }
                }
            }
        }
    }

    Mat makeTag(size_t id,
                size_t magnification)
    {
        Mat dst;
        makeTag(dst, id, magnification);

        return dst;
    }

    CheckSum            sumChecker;
    std::vector<size_t> perm, uperm;
    cv::Mat             tmp;
    cv::RNG             rng;
    unsigned long       m_seed;
    size_t              m_n;
    size_t              m_k;
    size_t              m_z;
};

TinyTagMaker::TinyTagMaker(size_t        n,
                           size_t        k,
                           unsigned long seed)
{
    m_d = new Private(n, k, seed);
}

TinyTagMaker::TinyTagMaker(const TinyTagMaker &i) :
    m_d(new Private(*i.m_d))
{ }

TinyTagMaker & TinyTagMaker::operator=(const TinyTagMaker &i)
{
    if (this != &i)
    {
        delete m_d;
        m_d = new Private(*i.m_d);
    }

    return *this;
}

TinyTagMaker::~TinyTagMaker()
{
    delete m_d;
}

void TinyTagMaker::makeTag(Mat   &dst,
                           size_t id,
                           size_t magnification)
{
    m_d->makeTag(dst, id, magnification);
}

cv::Mat TinyTagMaker::makeTag(size_t id,
                              size_t magnification)
{
    return m_d->makeTag(id, magnification);
}

void drawTags(cv::Mat                        &dst,
              const cv::Mat                  &src,
              const std::vector<TinyTagInfo> &tags)
{
    dst.create(src.size(), CV_8UC3);

    if (src.channels() == 1) { cvtColor(src, dst, COLOR_GRAY2BGR); }
    else { src.copyTo(dst); }

    vector<TinyTagInfo>::const_iterator it, ite = tags.end();
    for (it = tags.begin(); it != ite; ++it)
    {
        for (size_t i = 0; i < 4; ++i)
        {
            Point p0 = Point(static_cast<int>(it->corners()[i].x * CV_SHIFT),
                             static_cast<int>(it->corners()[i].y * CV_SHIFT));
            Point p1 = Point(static_cast<int>(it->center().x * CV_SHIFT),
                             static_cast<int>(it->center().y * CV_SHIFT));
#if CV_MAJOR_VERSION >= 3
            line(dst, p0, p1, Scalar(0, 255, 0), 1, LINE_AA, 16);
#else
            line(dst, p0, p1, Scalar(0, 255, 0), 1, CV_AA, 16);
#endif

            p1 = Point(static_cast<int>(it->corners()[(i + 1) % 4].x * CV_SHIFT),
                       static_cast<int>(it->corners()[(i + 1) % 4].y * CV_SHIFT));
#if CV_MAJOR_VERSION >= 3
            line(dst, p0, p1, (i == 0 ? Scalar(0, 0, 255) : Scalar(0, 255,
                                                                   0)), 1, LINE_AA,
                 16);
#else
            line(dst, p0, p1, (i == 0 ? Scalar(0, 0, 255) : Scalar(0, 255,
                                                                   0)), 1, CV_AA,
                 16);
#endif
        }
        string id = format("%d", it->id());
        putText(dst, id, Point(static_cast<int>(it->corners()[2].x + 10),
                               static_cast<int>(it->corners()[2].y + 10)),
                1, 1, Scalar(0, 255, 0));
    }
}

Mat drawTags(const Mat                 &src,
             const vector<TinyTagInfo> &tags)
{
    Mat dst;
    drawTags(dst, src, tags);

    return dst;
}
