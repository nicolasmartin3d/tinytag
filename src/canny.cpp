/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits>
#include <opencv2/core/core.hpp>
#if CV_MAJOR_VERSION >= 3
#include <opencv2/core/utility.hpp>
#endif
#include "canny.h"
/*M///////////////////////////////////////////////////////////////////////////////////////
 *   //
 *   //  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
 *   //
 *   //  By downloading, copying, installing or using the software you agree to this license.
 *   //  If you do not agree to this license, do not download, install,
 *   //  copy or use the software.
 *   //
 *   //
 *   //                        Intel License Agreement
 *   //                For Open Source Computer Vision Library
 *   //
 *   // Copyright (C) 2000, Intel Corporation, all rights reserved.
 *   // Third party copyrights are property of their respective owners.
 *   //
 *   // Redistribution and use in source and binary forms, with or without modification,
 *   // are permitted provided that the following conditions are met:
 *   //
 *   //   * Redistribution's of source code must retain the above copyright notice,
 *   //     this list of conditions and the following disclaimer.
 *   //
 *   //   * Redistribution's in binary form must reproduce the above copyright notice,
 *   //     this list of conditions and the following disclaimer in the documentation
 *   //     and/or other materials provided with the distribution.
 *   //
 *   //   * The name of Intel Corporation may not be used to endorse or promote products
 *   //     derived from this software without specific prior written permission.
 *   //
 *   // This software is provided by the copyright holders and contributors "as is" and
 *   // any express or implied warranties, including, but not limited to, the implied
 *   // warranties of merchantability and fitness for a particular purpose are disclaimed.
 *   // In no event shall the Intel Corporation or contributors be liable for any direct,
 *   // indirect, incidental, special, exemplary, or consequential damages
 *   // (including, but not limited to, procurement of substitute goods or services;
 *   // loss of use, data, or profits; or business interruption) however caused
 *   // and on any theory of liability, whether in contract, strict liability,
 *   // or tort (including negligence or otherwise) arising in any way out of
 *   // the use of this software, even if advised of the possibility of such damage.
 *   //
 *   //M*/

using namespace cv;

void cannyAuto(InputArray  _src,
               OutputArray _dst,
               double      percent_of_pixels_not_edges,
               int         aperture_size,
               bool        L2gradient)
{
    Mat src = _src.getMat();
    CV_Assert(src.depth() == CV_8U);

    _dst.create(src.size(), CV_8U);
    Mat dst = _dst.getMat();

    /*
     *   if (!L2gradient && (aperture_size & CV_CANNY_L2_GRADIENT) == CV_CANNY_L2_GRADIENT)
     *   {
     *    //backward compatibility
     *    aperture_size &= ~CV_CANNY_L2_GRADIENT;
     *    L2gradient = true;
     *   }
     */

    if (((aperture_size & 1) == 0) ||
        ((aperture_size != -1) && ((aperture_size < 3) || (aperture_size > 7))))
#if CV_MAJOR_VERSION >= 3
    { CV_Error(cv::Error::StsBadFlag, ""); }
#else
    { CV_Error(CV_StsBadFlag, ""); }
#endif

    cv::Mat dx(src.rows, src.cols, CV_16S);
    cv::Mat dy(src.rows, src.cols, CV_16S);

    cv::Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0, cv::BORDER_REPLICATE);
    cv::Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0, cv::BORDER_REPLICATE);

    int       low, high;
    ptrdiff_t mapstep    = src.cols + 2;
    size_t    bufferSize =
        static_cast<size_t>(mapstep * (src.rows + 2)) * (1 + sizeof(int));
    cv::AutoBuffer<uchar> buffer(bufferSize);

    int *mag_buf = reinterpret_cast<int *>(static_cast<uchar *>(buffer));
    memset(mag_buf, 0, static_cast<size_t>(mapstep) * sizeof(int));

    uchar *map = reinterpret_cast<uchar *>(mag_buf + mapstep * (src.rows + 2));
    memset(map, 1, static_cast<size_t>(mapstep));
    memset(map + static_cast<size_t>(mapstep * (src.rows + 1)), 1,
           static_cast<size_t>(mapstep));

    size_t maxsize = std::max(static_cast<size_t>(1 << 10),
                              static_cast<size_t>(src.cols * src.rows / 10));
    std::vector<uchar *> stack(maxsize);

    uchar **stack_top    = &stack[0];
    uchar **stack_bottom = &stack[0];

    #define CANNY_PUSH(d)    *(d) = uchar(2), *stack_top++ = (d)
    #define CANNY_POP(d)     (d)  = *--stack_top

    // calculate magnitude and angle of gradient, perform non-maxima supression.
    // fill the map with one of the following values:
    //   0 - the pixel might belong to an edge
    //   1 - the pixel can not belong to an edge
    //   2 - the pixel does belong to an edge
    int minV = std::numeric_limits<int>::max();
    int maxV = 0;
    for (int i = 0; i <= src.rows; i++)
    {
        int *_norm = &mag_buf[(i + 1) * mapstep + 1];
        if (i < src.rows)
        {
            short *_dx = dx.ptr<short>(i);
            short *_dy = dy.ptr<short>(i);

            if (!L2gradient)
            {
                for (int j = 0; j < src.cols; j++)
                {
                    _norm[j] = std::abs(int(_dx[j])) + std::abs(int(_dy[j]));
                    if (_norm[j] > maxV) { maxV = _norm[j]; }
                    if (_norm[j] < minV) { minV = _norm[j]; }
                }
            }
            else
            {
                for (int j = 0; j < src.cols; j++)
                {
                    _norm[j] = int(_dx[j]) * _dx[j] + int(_dy[j]) * _dy[j];
                    if (_norm[j] > maxV) { maxV = _norm[j]; }
                    if (_norm[j] < minV) { minV = _norm[j]; }
                }
            }

            _norm[-1] = _norm[src.cols] = 0;
        }
        else
        {
            memset(_norm - 1, 0, static_cast<size_t>(mapstep) * sizeof(int));
        }
    }

    // step 2: Get the histogram of the data.
#define NUM_BINS 256
    // might want to make this max - min / NUM_BINS after you have normalized.
    // int bin_size = (maxV-minV) / NUM_BINS;
    // if (bin_size < 1) bin_size = 1;
    double bin_size = std::max(double(maxV - minV) / NUM_BINS, 1.0);
    int    bins[NUM_BINS + 1];
    std::fill(bins, bins + NUM_BINS + 1, 0);

    for (int i = 0; i < src.rows; i++)
    {
        int *_norm = &mag_buf[(i + 1) * mapstep + 1];
        for (int j = 0; j < src.cols; j++)
        {
            bins[int((_norm[j] - minV) / bin_size)]++;
        }
    }

    // step 3: get the high threshold
    double threshold_ratio = 0.4;
    int    total           = 0;
    high = 0;
    // size.height should be here too, but right now we're going row-by-row
    while (total < src.rows * src.cols * percent_of_pixels_not_edges)
    {
        total += bins[high];
        high++;
    }

    high = static_cast<int>(cvRound(high * bin_size));
    low  = static_cast<int>(cvRound(threshold_ratio * high));

    // printf("lo %d hi %d\n", low, high);

    for (int i = 1; i <= src.rows; i++)
    {
        uchar *_map = map + mapstep * i + 1;
        _map[-1] = _map[src.cols] = 1;

        int         *_mag = &mag_buf[i * mapstep + 1];
        const short *_x   = dx.ptr<short>(i - 1);
        const short *_y   = dy.ptr<short>(i - 1);

        if (static_cast<size_t>((stack_top - stack_bottom) + src.cols) >
            maxsize)
        {
            int sz = static_cast<int>(stack_top - stack_bottom);
            maxsize = maxsize * 3 / 2;
            stack.resize(maxsize);
            stack_bottom = &stack[0];
            stack_top    = stack_bottom + sz;
        }

        int prev_flag = 0;
        for (int j = 0; j < src.cols; j++)
        {
            #define CANNY_SHIFT 15
            const int TG22 =
                static_cast<int>(0.4142135623730950488016887242097 *
                                 (1 << CANNY_SHIFT) + 0.5);

            int m = _mag[j];

            if (m > low)
            {
                int xs = _x[j];
                int ys = _y[j];
                int x  = std::abs(xs);
                int y  = std::abs(ys) << CANNY_SHIFT;

                int tg22x = x * TG22;

                if (y < tg22x)
                {
                    if ((m > _mag[j - 1]) &&
                        (m >= _mag[j + 1])) { goto __ocv_canny_push; }
                }
                else
                {
                    int tg67x = tg22x + (x << (CANNY_SHIFT + 1));
                    if (y > tg67x)
                    {
                        if ((m > _mag[j - mapstep]) &&
                            (m >= _mag[j + mapstep])) { goto __ocv_canny_push; }
                    }
                    else
                    {
                        int s = (xs ^ ys) < 0 ? -1 : 1;
                        if ((m > _mag[j - mapstep - s]) &&
                            (m >
                             _mag[j + mapstep + s])) { goto __ocv_canny_push; }
                    }
                }
            }
            prev_flag = 0;
            _map[j]   = uchar(1);
            continue;
        __ocv_canny_push:
            if (!prev_flag && (m > high) && (_map[j - mapstep] != 2))
            {
                CANNY_PUSH(_map + j);
                prev_flag = 1;
            }
            else
            {
                _map[j] = 0;
            }
        }
    }

    // now track the edges (hysteresis thresholding)
    while (stack_top > stack_bottom)
    {
        uchar *m;
        if (static_cast<size_t>((stack_top - stack_bottom) + 8) > maxsize)
        {
            int sz = static_cast<int>(stack_top - stack_bottom);
            maxsize = maxsize * 3 / 2;
            stack.resize(maxsize);
            stack_bottom = &stack[0];
            stack_top    = stack_bottom + sz;
        }

        CANNY_POP(m);

        if (!m[-1])           { CANNY_PUSH(m - 1);           }
        if (!m[1])            { CANNY_PUSH(m + 1);           }
        if (!m[-mapstep - 1]) { CANNY_PUSH(m - mapstep - 1); }
        if (!m[-mapstep])     { CANNY_PUSH(m - mapstep);     }
        if (!m[-mapstep + 1]) { CANNY_PUSH(m - mapstep + 1); }
        if (!m[mapstep - 1])  { CANNY_PUSH(m + mapstep - 1); }
        if (!m[mapstep])      { CANNY_PUSH(m + mapstep);     }
        if (!m[mapstep + 1])  { CANNY_PUSH(m + mapstep + 1); }
    }

    // the final pass, form the final image
    const uchar *pmap = map + mapstep + 1;
    uchar       *pdst = dst.ptr();
    for (int i = 0; i < src.rows; i++, pmap += static_cast<size_t>(mapstep),
         pdst += static_cast<size_t>(dst.step))
    {
        for (size_t j = 0; j < static_cast<size_t>(src.cols); j++)
        {
            pdst[j] = static_cast<uchar>(-(pmap[j] >> 1));
        }
    }
}

/* End of file. */
