/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CRC16_HPP
#define CRC16_HPP
class CRC16
{
    public:
        CRC16(unsigned long poly=0x1021);
        CRC16(const CRC16 &i);
        CRC16 & operator=(const CRC16 &i);
        ~CRC16();

        unsigned short checksum(unsigned long value);

    private:
        struct Private;
        Private *m_d;
};

#endif /* #ifndef CRC16_HPP */
