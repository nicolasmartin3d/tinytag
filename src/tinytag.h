/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TINYTAG
#define TINYTAG

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include "export.h"

class TINYTAG_DECLSPEC TinyTagInfo
{
    public:
        TinyTagInfo(cv::Point2f              center,
                    std::vector<cv::Point2f> corners,
                    int                      id);
        TinyTagInfo(const TinyTagInfo &i);
        TinyTagInfo & operator=(const TinyTagInfo &i);
        ~TinyTagInfo();

        cv::Point2f center() const;
        const std::vector<cv::Point2f> & corners() const;
        int id() const;

        double approxArea() const;

    private:
        struct Private;
        Private *m_privImpl;

};

class TINYTAG_DECLSPEC TinyTagDetector
{
    public:
        TinyTagDetector(size_t        n=5,
                        size_t        k=9,
                        unsigned long seed=0x27d2);
        TinyTagDetector(const TinyTagDetector &i);
        TinyTagDetector & operator=(const TinyTagDetector &i);
        ~TinyTagDetector();

        // int identifyTag(const cv::Mat &src);
        int identifyTag(const cv::Mat &src,
                        int           &order);

        void detectTags(std::vector<TinyTagInfo> &tags,
                        const cv::Mat            &src);

        bool sanityCheck();

        // convenient function that creates a vector of tags instead of reusing one
        std::vector<TinyTagInfo> detectTags(const cv::Mat &src);

    private:
        struct Private;
        Private *m_d;
};

class TINYTAG_DECLSPEC TinyTagMaker
{
    public:
        TinyTagMaker(size_t        n=5,
                     size_t        k=9,
                     unsigned long seed=0x27d2);
        TinyTagMaker(const TinyTagMaker &i);
        TinyTagMaker & operator=(const TinyTagMaker &i);
        ~TinyTagMaker();

        void makeTag(cv::Mat &dst,
                     size_t   id,
                     size_t   magnification=1);

        // convenient function that creates an image instead of reusing one
        cv::Mat makeTag(size_t id,
                        size_t magnification=1);

    private:
        struct Private;
        Private *m_d;
};

TINYTAG_DECLSPEC void drawTags(cv::Mat                        &dst,
                               const cv::Mat                  &src,
                               const std::vector<TinyTagInfo> &tags);

// convenient function that creates an image instead of reusing one
TINYTAG_DECLSPEC cv::Mat drawTags(const cv::Mat                  &src,
                                  const std::vector<TinyTagInfo> &tags);

#endif
