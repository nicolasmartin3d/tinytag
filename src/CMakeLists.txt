#
# TinyTag - A library to detect and print simple fiducial markers.
# Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
#
# This file is part of TinyTag.
#
# TinyTag is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# TinyTag is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
#

file(GLOB tinytag_srcs RELATIVE ${CMAKE_SOURCE_DIR} *.cpp)
file(GLOB tinytag_hdrs RELATIVE ${CMAKE_SOURCE_DIR} *.h *.tpp)

# headers that won't be made public
set(tinytag_public_hdrs ${tinytag_hdrs})
list(REMOVE_ITEM tinytag_public_hdrs src/canny.h src/crc16.h)

set(TINYTAG_SOURCE_FILES ${tinytag_srcs} ${tinytag_hdrs} CACHE INTERNAL "Source files of TinyTag.")
set(TINYTAG_HEADER_FILES ${tinytag_public_hdrs} CACHE INTERNAL "Header files of TinyTag.")
