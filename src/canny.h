/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CANNY_AUTO_H
#define CANNY_AUTO_H

#include <opencv2/imgproc/imgproc.hpp>

void cannyAuto(cv::InputArray  image,
               cv::OutputArray edges,
               double          percent_of_pixels_not_edges=0.8,
               // double low_thresh, double high_thresh,
               int             apertureSize=3,
               bool            L2gradient=false);

#endif
