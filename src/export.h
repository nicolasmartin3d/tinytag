/*
 * TinyTag - A library to detect and print simple fiducial markers.
 * Copyright (C) 2012-2015 Nicolas Martin (nicolas.martin.3d@gmail.com)
 *
 * This file is part of TinyTag.
 *
 * TinyTag is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * TinyTag is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with TinyTag.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Inspired by : OpenThreads library, Copyright (C) 2002 - 2007  The Open Thread Group
 */

#include "config.h"

#ifndef OS_WIN
    #define TINYTAG_DECLSPEC
#else
    #if defined(TINYTAG_STATIC_LIBRARY)
        #define TINYTAG_DECLSPEC
    #elif defined(TINYTAG_BUILDING)
        #define TINYTAG_DECLSPEC __declspec(dllexport)
    #else
        #define TINYTAG_DECLSPEC __declspec(dllimport)
    #endif
#endif
