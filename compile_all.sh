#! /bin/bash

configs[1]=linux_static_x64_noQt
prefixs[1]=/home/CodedLight_v2.1_proto2/Install/linux/static/Release/x64
opts_shared[1]=OFF

configs[2]=linux_static_x64_Qt
prefixs[2]=/home/CodedLight_v2.1_proto2/Install/linux_qt/static/Release/x64
opts_shared[2]=OFF

configs[3]=linux_static_in_shared_x64_noQt
prefixs[3]=/home/CodedLight_v2.1_proto2/Install/linux/shared/Release/x64
opts_shared[3]=OFF

configs[4]=linux_shared_x64_Qt
prefixs[4]=/home/CodedLight_v2.1_proto2/Install/linux_qt/shared/Release/x64
opts_shared[4]=ON

mkdir -p ../../Build/tinytag
cd ../../Build/tinytag

for i in $(seq 1 4); do 
	c=${configs[$i]}
    rm -rf $c
    mkdir $c
    cd $c

	p=${prefixs[$i]}
	cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$p \
	 -DMVG_DIR=$p/share \
	 -DXTCLAP_INCLUDE_DIR=$p/include \
	 -DBUILD_EXAMPLES=ON -DBUILD_SHARED_LIBS=${opts_shared[$i]} ../../../Source/tinytag
	 
    make -j4
    make install
	
	cd ..
done
